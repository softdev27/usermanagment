/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService.addUser("user2", "password");
        System.out.println(UserService.getUser());
        UserService.addUser(new User("user3", "password"));
        System.out.println(UserService.getUser());
        
        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("1234");
        UserService.updateUser(3, user);
        System.out.println(UserService.getUser());
        
        UserService.addUser(user);
        System.out.println(UserService.getUser());
        
        System.out.println(UserService.login("admin", "password"));
    }
}
