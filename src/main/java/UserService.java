
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    // Mockup
    static {
        userList.add(new User("admin", "password"));
       userList.add(new User("user1", "password"));
    }
    // create
    public static boolean addUser(User user){
        userList.add(user);
        return true;
    }
    
    public static boolean addUser(String userName, String password){
        userList.add(new User(userName, password));
        return true;
    }
    //update
    public static boolean updateUser(int index, User user){
        userList.set(index, user);
        return true;
    }
    // Read 1 user
    public static User getUser(int index){
        if(index>userList.size()-1){
            return null;
        }
        return userList.get(index);
    }
    // Read all user
    public static ArrayList<User> getUser(){
        return userList;
    }
    // Read all user
    public static ArrayList<User> searcUserName(String searchText){
        ArrayList<User> list = new ArrayList<>();
        for(User user: userList){
            if(user.getUsername().startsWith(searchText))
                list.add(user);
        }
        return list;
    }
    // Delete user
    public static boolean delUser(int index){
        userList.remove(index);
        return true;
    }
    
    // Login
    public static User login(String userName, String password){
        for(User user: userList){
            if(user.getUsername().equals(userName) && user.getPassword().equals(password)){
                return user; 
            }
        }
        return null;
    }
     public static void save() {
         FileOutputStream fos = null;
        try {
           
            // Friends.dat
            File file = new File("Users.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
               
            }
        }
     }
     
     public static void load() {
    FileInputStream fis = null;
        try {
            File file = new File("Users.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);;
            ArrayList<User> user = (ArrayList) ois.readObject();
            
           System.out.println(user);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
            
        } finally {
            try {
                if(fis!=null){
                    fis.close();
                }
                
            } catch (IOException ex) {
                
            }
        }   
            
           
}
}
